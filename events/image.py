import requests

url = 'https://api.github.com/some/endpoint'
headers = {
    'Authorization':
    'kxCPfHA5OrPjVZrpnKjG4g18SJiFO67wzuaXc5TxMwS2v85fdOUl3ei7'
    }

response = requests.get(url, headers=headers)

data = response.json()

photo = {"original_url": data["photos"][0]["src"]["original"]}
print(photo)
